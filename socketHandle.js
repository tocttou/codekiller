var fs = require('fs');

var getOutput = function (socket, code, langMode, stdin) {

    var fileName, command;
    var stdin = ' ' + stdin;

    switch (langMode) {
        case 'c':
            fileName = "sample.c";
            command = "gcc -o sample /tmp/sample.c && ./sample" + stdin;
            break;
        case 'cpp':
            fileName = "sample.cpp";
            command = "g++ -o sample /tmp/sample.cpp && ./sample" + stdin;
            break;
        case 'java':
            fileName = "Main.java";
            command = "javac /tmp/Main.java && cd /tmp && java Main" + stdin;
            break;
        case 'javascript':
            fileName = "sample.js";
            command = "node /tmp/sample.js" + stdin;
            break;
        case 'python':
            fileName = "sample.py";
            command = "python /tmp/sample.py" + stdin;
            break;
        case 'python3':
            fileName = "sample.py";
            command = "python3 /tmp/sample.py" + stdin;
            break;
    }

    fs.writeFile("/tmp/" + fileName, code, function(err) {
        if(err) {
            return console.log(err);
        }
        console.log("The file was saved!");
    });

    var spawn = require('child_process').spawn;
    var output = spawn('docker', ['run', '-v',
            '/tmp:/tmp', 'codekillerbase',
            '/bin/bash', '-c',
            command
        ]
    );

    output.stdout.on('data', function (data) {
        socket.emit('setOutput', data);
        console.log('stdout happened');
    });;

    output.stderr.on('data', function (data) {
        socket.emit('setError', data);
    });

    output.on('close', function () {
        socket.emit('outputClosed', '');
    });
};

var transactions = function (server) {
    var io = require('socket.io').listen(server);

    io.sockets.on('connection', function (socket) {

        console.log('connected!');

        socket.on('getOutput', function (code, langMode, stdin) {
            getOutput(socket, code, langMode, stdin);
        });

        socket.on('disconnect', function(){
            console.log('disconnected');
        });

    });
};

module.exports = transactions;