// initialise the editor
var editor = ace.edit("editor");
editor.setTheme("ace/theme/twilight");
editor.session.setMode("ace/mode/" + langMode);
editor.setFontSize(15);

// make connection with socket
// bindTo is injected into views/index.hbs
// from config/config.js
var socket = io.connect(bindTo);


// get permalink
$("#get-permalink").on('click', function () {

    $.post( "/permalink",
        {
            code: editor.getValue(),
            langMode: window.location.pathname.split('/')[1]
        })
        .done(function (data) {
            $("#permalink-text")
                .css("visibility", "visible")
                .val(data.output);
        });
});

// reset editor
$("#reset-editor").on('click', function () {
    editor.setValue("");
});

// erase output
$("#output-eraser").on('click', function () {
    $("#output-text").text('');
});

// run code
socket.on('connect', function(){

    $("#runner").on('click', function () {

        var code = editor.getValue();

        $(".loader").css("visibility", "visible");
        socket.emit('getOutput', code,
            window.location.pathname.split('/')[1],
            $("#input-args").val()
        );

    });

    socket.on('setOutput', function (data) {
        $(".loader").css("visibility", "hidden");
        $("#output-text").append(
            ab2str(data)
                .replace(/ /g, '\u00a0')
                .replace(/\n/g, "<br />")
        );
    });

    socket.on('setError', function (data) {
        $(".loader").css("visibility", "hidden");
        $("#output-text").append(
            "<span style='color: red'> <br /> ERROR!! :: "
            + ab2str(data)
                .replace(/ /g, '\u00a0')
                .replace(/\n/g, "<br />")
            + "<br /></span>"
        );
    });

    socket.on('outputClosed', function (data) {
        $(".loader").css("visibility", "hidden");
    });

});

// helper functions
function ab2str(buf) {
    return String.fromCharCode.apply(null, new Uint8Array(buf));
}

