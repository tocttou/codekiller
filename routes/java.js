var express = require('express');
var router = express.Router();
var os = require('os');
var MongoClient = require('mongodb').MongoClient;
var config = require('../config/config.js');
var auth = require('../auth');

var getCode = function (req, res, next) {
    var codeId = req.baseUrl.split('/')[2];
    req.codeId = codeId;

    if (codeId !== '0') {
        var url = 'mongodb://localhost:27017/codez';

        MongoClient.connect(url, function(err, db) {

            db.collection('codez')
                .find({ 'crc32' : codeId })
                .toArray(function (err, doc) {
                    if (doc.length != 0) {
                        req.precode = doc[0].code;
                        db.close();
                    }
                    next();
                });
        });

    } else {
        next();
    }
};

// GET home page
router.get('/', auth, getCode, function (req, res) {
    if (typeof req.precode === 'undefined') {
        if(req.codeId == 0) {
            req.precode = "import java.util.*;\n" +
            "import java.lang.*;\n" +
            "import java.io.*;\n" +
            "\n" +
            "/* Name of the public class has to be 'Main' */\n" +
            "public class Main\n" +
            "{\n" +
            "    public static void main (String[] args) throws java.lang.Exception\n" +
            "       {\n" +
            '           System.out.println("Hell world from Java!");\n' +
            "       }\n" +
        "};"
        } else {
            req.redirection = true;
        }
    }

    if (req.redirection === true) {
        res.redirect('/java/0');
    } else {
        res.render('index',
            {
                title: 'Try java Online',
                lang: 'java-8-openjdk-amd64',
                langMode: "'" + 'java' + "'",
                preCode: req.precode,
                numCores: os.cpus().length > 1 ? os.cpus().length : "Single",
                bindTo: "'" + config.bindAddress + ":"
                +  config.bindPort + "'"
            });
    }
});


module.exports = router;
