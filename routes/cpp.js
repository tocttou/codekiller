var express = require('express');
var router = express.Router();
var os = require('os');
var MongoClient = require('mongodb').MongoClient;
var config = require('../config/config.js');
var auth = require('../auth');

var getCode = function (req, res, next) {
    var codeId = req.baseUrl.split('/')[2];
    req.codeId = codeId;

    if (codeId !== '0') {
        var url = 'mongodb://localhost:27017/codez';

        MongoClient.connect(url, function(err, db) {

            db.collection('codez')
                .find({ 'crc32' : codeId })
                .toArray(function (err, doc) {
                    if (doc.length != 0) {
                        req.precode = doc[0].code;
                        db.close();
                    }
                    next();
                });
        });

    } else {
        next();
    }
};

// GET home page
router.get('/', auth, getCode, function (req, res) {
    if (typeof req.precode === 'undefined') {
        if(req.codeId == 0) {
            req.precode = "#include<iostream>\n" +
            "using namespace std;\n" +
            "\n" +
            "int main()\n" +
            "{\n" +
            '    cout << "Hello World!";\n' +
            "    return 0;\n" +
            "};\n"
        } else {
            req.redirection = true;
        }
    }

    if (req.redirection === true) {
        res.redirect('/cpp/0');
    } else {
        res.render('index',
            {
                title: 'Try C++ Online',
                lang: 'gcc (Ubuntu 5.2.1-22ubuntu2) 5.2.1 20151010',
                langMode: "'" + 'c_cpp' + "'",
                preCode: req.precode,
                numCores: os.cpus().length > 1 ? os.cpus().length : "Single",
                bindTo: "'" + config.bindAddress + ":"
                +  config.bindPort + "'"
            });
    }
});


module.exports = router;
