var express = require('express');
var router = express.Router();
var auth = require('../auth');
var config = require('../config/config.js');

// GET home page
router.get('/', auth, function (req, res) {

    res.render('default',
        {
            title: 'Try Languages Online',
            link: config.bindAddress + ':' +config.bindPort
        });
});


module.exports = router;