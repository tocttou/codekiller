var express = require('express');
var bodyParser = require("body-parser");
var router = express.Router();
var auth = require('../auth');
var config = require('../config/config.js');
var MongoClient = require('mongodb').MongoClient;
var CRC32 = require('crc-32');

router.use(bodyParser.urlencoded({ extended: true }));

var createNewPermalink = function (req, res, next) {

    var code = req.body.code;
    var langMode = req.body.langMode;
    var url = 'mongodb://localhost:27017/codez';

    var crc32 = Math.abs(CRC32.str(code));

    MongoClient.connect(url, function(err, db) {

        db.collection('codez')
            .find({ 'crc32' : crc32 })
            .toArray(function (err, doc) {
                if (doc.length != 0) {
                    db.close();
                } else {
                    db.collection('codez').insertOne( {
                        crc32: crc32.toString(),
                        code: code
                    },  function(err, result) {
                        console.log("inserted a new entry in codez.codez");
                    });
                }
                req.permurl = config.bindAddress + ':'
                    + config.bindPort + '/' + langMode +'/' + crc32;
                next();
            });
    });
};

router.post('/', auth, createNewPermalink, function (req, res) {
    res.send({
        output: req.permurl
    });
});

module.exports = router;