var express = require('express');
var router = express.Router();
var os = require('os');
var MongoClient = require('mongodb').MongoClient;
var config = require('../config/config.js');
var auth = require('../auth');

var getCode = function (req, res, next) {
    var codeId = req.baseUrl.split('/')[2];
    req.codeId = codeId;

    if (codeId !== '0') {
        var url = 'mongodb://localhost:27017/codez';

        MongoClient.connect(url, function(err, db) {

            db.collection('codez')
                .find({ 'crc32' : codeId })
                .toArray(function (err, doc) {
                    if (doc.length != 0) {
                        req.precode = doc[0].code;
                        db.close();
                    }
                    next();
                });
        });

    } else {
        next();
    }
};

// GET home page
router.get('/', auth, getCode, function (req, res) {
    if (typeof req.precode === 'undefined') {
        if(req.codeId == 0) {
            req.precode = 'console.log("Hello world from nodejs!");';
        } else {
            req.redirection = true;
        }
    }

    if (req.redirection === true) {
        res.redirect('/javascript/0');
    } else {
        res.render('index',
            {
                title: 'Try nodejs Online',
                lang: 'nodejs ' + process.version,
                langMode: "'" + 'javascript' + "'",
                preCode: req.precode,
                numCores: os.cpus().length > 1 ? os.cpus().length : "Single",
                bindTo: "'" + config.bindAddress + ":"
                +  config.bindPort + "'"
            });
    }
});


module.exports = router;
