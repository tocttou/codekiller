var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var config = require('./config/config.js');
var fs = require('fs');

var app = express();

var indexRoute = require('./routes/index.js')
var python = require('./routes/python');
var python3 = require('./routes/python3');
var javascript = require('./routes/javascript');
var java = require('./routes/java');
var cpp = require('./routes/cpp');
var c = require('./routes/c');
var permalink = require('./routes/createPermalink');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

// middleware setup
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(require('node-sass-middleware')({
    src: path.join(__dirname, 'public'),
    dest: path.join(__dirname, 'public'),
    indentedSyntax: true,
    sourceMap: true
}));
app.use(express.static(path.join(__dirname, 'public')));

// set default routes
app.use('/', indexRoute);
app.use('/python/:id', python);
app.use('/python3/:id', python3);
app.use('/javascript/:id', javascript);
app.use('/java/:id', java);
app.use('/cpp/:id', cpp);
app.use('/c/:id', c);
app.use('/permalink', permalink);

// set redirect to /<lang>/0
app.get('/python', function (req, res) {
    res.redirect('/python/0');
});
app.get('/python3', function (req, res) {
    res.redirect('/python3/0');
});
app.get('/javascript', function (req, res) {
    res.redirect('/javascript/0');
});
app.get('/java', function (req, res) {
    res.redirect('/java/0');
});
app.get('/cpp', function (req, res) {
    res.redirect('/cpp/0');
});
app.get('/c', function (req, res) {
    res.redirect('/c/0');
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function(err, req, res) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

module.exports = app;