## Run languages online

Important: Dockerfile [cd to Docker]: `docker build -t codekillerbase .`
Important: For permalink feature, you must have a db 'codez' in mongodb at default port [optionally use a docker image: see below]

## Setup
1. Copy config/config_sample.js to config/config.js and edit accordingly [use bindAddress: localhost or 0.0.0.0 locally and public IP of the system on production
2. run `npm install`
3. build the mongodb docker container: [cd to mongodb-docker]: `docker build -t mongodb .` | alternatively you can run the daemon natively
4. run the mongodb container as a daemon `docker run -d -p 27017:27107 mongodb` [note that this mongodb instance is unprotected as of now]
5. run the app by `node bin/www`